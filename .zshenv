# Set correct / autocorrect
setopt correct
unsetopt correctall
# # I'm opinionated on history
setopt append_history
setopt extended_history
setopt hist_expire_dups_first
setopt hist_ignore_all_dups
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt hist_verify

# Share your history across all your terminal windows
setopt share_history
setopt noclobber

#
# set some more options
setopt AUTO_PUSHD
setopt AUTO_CD
setopt MULTIOS
setopt pushd_ignore_dups
