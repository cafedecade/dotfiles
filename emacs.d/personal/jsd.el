;;; jsd.el -- Provides normal customizations for bbatsov
;;; Commentary:

;;
;; Remove prelude-guru-mode


;;; Code:


(prelude-require-package 'git-gutter)
(prelude-require-package 'ag)
(prelude-require-package 'pt)

(prelude-require-package 'go-snippets)
(prelude-require-package 'yasnippet)

(prelude-require-package 'terraform-mode)

(prelude-require-package 'sublime-themes)
(prelude-require-package 'color-theme-modern)
(prelude-require-package 'base16-theme)

(setq prelude-guru nil)

(setq default-frame-alist '( (left . 680) (top . 80) (height . 90) (width . 155) ))

;; Git gutter configuration ----------------------------------------------
(require 'git-gutter)
(global-git-gutter-mode t)
(setq git-gutter:modified-sign "~")
(setq git-gutter:added-sign    "+")
(setq git-gutter:deleted-sign  "-")

(set-face-foreground 'git-gutter:modified "purple")
(set-face-foreground 'git-gutter:added "green")
(set-face-foreground 'git-gutter:deleted "red")

;; Stage current hunk
(global-set-key (kbd "C-x v s") 'git-gutter:stage-hunk)

;; Revert current hunk
(global-set-key (kbd "C-x v r") 'git-gutter:revert-hunk)
;; Git gutter configuration end. ----------------------------------------

;;; Whitespace settings.
(setq whitespace-line-column 100)
;; Ok Just turn off whitespace
(setq prelude-whitespace nil)

;; Swap Control and Super on osx.
(if (eq system-type 'darwin)
    (progn
      (setq mac-command-modifier 'meta)
      (setq mac-option-modifier 'super)
      ))
;; Turn off whitespace mode in programming mdoes.
;;(add-hook 'prog-mode-hook 'prelude-turn-off-whitespace t) (server-start)

(setenv "GOPATH" "/Users/joe/go")

;; Start a daemon server
(server-start)
(load-theme 'spolsky)

(global-set-key (kbd "M-z") 'undo)
(global-set-key (kbd "S-z") 'undo-tree-map)
(global-set-key (kbd "C-x u") 'undo)

;; (global-linum-mode t)
(provide 'jsd)
;;; jsd.el ends here
