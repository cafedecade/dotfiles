;;; jsd-preload.el -- Provides preload customizations for bbatsov
;;; Commentary:
;; Add gitgutter config



;;; Code:


;;;(setq prelude-theme 'dark-laptop)


;; (if (custom-theme-p 'spolsky)
;;     (setq prelude-theme 'spolsky)
;;  (setq prelude-theme 'nil))

(require 'prelude-packages)
(setq default-frame-alist '((font . "SF Mono-12")))
(set-frame-font "SF Mono-12" t t)

(add-to-list 'custom-theme-load-path "~/.emacs.d/elpa/color-theme-modern-20160411.1846")
(add-to-list 'custom-theme-load-path "~/.emacs.d/elpa/sublime-themes-20160111.122")
(add-to-list 'custom-theme-load-path "/Users/joe/.emacs.d/themes")
(setq-default prelude-theme 'base16-twilight-dark)

;;(setq prelude-theme 'spolsky) ;; Load Spolsky will fail on first run through.

(setq prelude-guru-mode nil)

(setq mac-command-modifier 'meta)
(setq mac-option-modifier 'super)

(provide 'jsd-preload)
;;; jsd-preload.el ends here

