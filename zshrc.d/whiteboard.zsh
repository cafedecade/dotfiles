whiteboard() {
  convert "$1" "$1.converted.png"
  echo "Converted"
  convert "$1.converted.png" \
    -morphology Convolve DoG:15,100,0 \
    -negate -normalize -blur 0x1 -channel RBG \
    -level 60%,95%,0.1 \
    "$1.cleaned.png"
  echo "Cleaned"
  autotrace \
    --dpi 1024 \
    --line-threshold 0.1 \
    --color-count 16 \
    --corner-always-threshold 60 \
    --line-reversion-threshold 0.1 \
    --width-weight-factor 0.1 \
    --despeckle-level 10 \
    --despeckle-tightness 5 \
    --preserve-width \
    --remove-adjacent-corners \
    --output-format svg \
    --output-file "$1.cleaned.svg" \
    "$1.cleaned.png"
  pngcrush -ow -q "$1.cleaned.png"
  echo "SVG traced"
}
