########################################
## Setup Misc.
########################################

export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home/
if [ -f /usr/libexec/java_home ]; then 
  export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
  export TERM=xterm-256color
fi
