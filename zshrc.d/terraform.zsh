alias tfp="terraform plan -out terraform.tfplan"
alias tfa="terraform apply"
alias tfd="terraform destroy"
alias tfg="terraform graph -draw-cycles | dot -Tpng > graph2.png"
