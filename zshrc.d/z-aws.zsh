compinit 

if [ -f /usr/local/share/zsh/site-functions/_aws ]; then
      source /usr/local/share/zsh/site-functions/_aws
fi

AMAZON_HVM_AMI_IMAGE="ami-60b6c60a"
UBUNTU_HVM_AMI_IMAGE="ami-d05e75b8"

AMAZON_PV_AMI_IMAGE="ami-5fb8c835"
UBUNTU_PV_AMI_IMAGE="ami-d85e75b0"
