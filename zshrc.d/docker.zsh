# Docker

function dme() {
  eval "$(docker-machine env "${1:-default}")"
}

function docker-clean() {
  docker rmi -f $(docker images -q -a -f dangling=true)
}

alias dm="docker-machine"
alias ds="docker-swarm"
alias dc="docker-compose"

function mkdroplet() {
    docker-machine create --driver digitalocean --digitalocean-size 4g --digitalocean-region sfo1 --digitalocean-image docker {$1:-droplet}
}

alias mkdockaws='docker-machine create --driver amazonec2'
alias mkdockgoog='docker-machine create --driver google --google-project docker-test-weave-1 --google-machine-type "n1-standard-4" --google-disk-size "100"'
alias mkdockdrop="docker-machine create --driver digitalocean --digitalocean-size 4gb --digitalocean-image docker"
alias mkdockazure='docker-machine create -d azure  --azure-location "Central US"'
alias mkdockvcair='docker-machine create -d vmwarevcloudair '
#alias rmdockdo="docker-machine rm -y $(docker-machine ls | grep digitalocean | awk '{print $1}' | xargs)"
#lias rmdockaws="docker-machine rm -y $(docker-machine ls | grep amazonec2  | awk '{ print $1}' | xargs)"

alias de="env | grep DOCKER_"
alias dm-list="docker-machine ls"

# ------------------------------------
# Docker alias and function
# ------------------------------------

# Get latest container ID
alias dl="docker ps -l -q"

alias dco="docker-compose"

# Get container process
alias dps="docker ps"

# Get process included stop container
alias dpa="docker ps -a"

# Get images
alias di="docker images"

# Get container IP
alias dip="docker inspect --format '{{ .NetworkSettings.IPAddress }}'"

# Run deamonized container, e.g., $dkd base /bin/echo hello
alias dkd="docker run -d -P"

# Run interactive container, e.g., $dki base /bin/bash
alias dki="docker run -i -t -P"

# Execute interactive container, e.g., $dex base /bin/bash
alias dex="docker exec -i -t"

# Stop all containers
dstop() { docker stop $(docker ps -a -q); }

# Remove all containers
drm() { docker rm $(docker ps -a -q); }

# Stop and Remove all containers
alias drmf='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'

# Remove all images
dri() { docker rmi $(docker images -q); }

# Dockerfile build, e.g., $dbu tcnksm/test
dbu() { docker build -t=$1 .; }

# Show all alias related docker
dalias() { alias | grep 'docker' | sed "s/^\([^=]*\)=\(.*\)/\1 => \2/"| sed "s/['|\']//g" | sort; }

docker-ip() {  
    docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$@"
}

