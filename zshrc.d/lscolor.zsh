#
# Set LSCOLORS
#
#export LSCOLORS="cxfxcxdxbxGxDxabagacad"
export LSCOLORS="gxfxcxdxbxeggfabagacgd"
export LS_COLORS="di=36;40:ln=35;40:so=32;40:pi=33;40:ex=31;40:bd=34;46:cd=36;45:su=0;41:sg=0;46:tw=0;42:ow=36;43:"
