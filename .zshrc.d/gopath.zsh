# path, the 0 in the filename causes this to load first

export GOPATH="$HOME/go"

path=(
  $GOPATH/bin
  $path
  /usr/local/go/bin
)
