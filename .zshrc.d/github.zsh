function github-ls() {
    if [ "$1" = "org" ]; then
        curl -s -H "Authorization: token $GITHUB_TOKEN" https://api.github.com/orgs/iq4health/repos\?per_page\=100 | grep -o 'git@[^"]*'
    else
        curl -s -H "Authorization: token $GITHUB_TOKEN" https://api.github.com/users/jduhamel/repos\?per_page\=100 | grep -o 'git@[^"]*'
    fi
}
