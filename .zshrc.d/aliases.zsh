unalias gci
#unalias scp
alias gci="gcloud compute instances"
unalias gb
alias aws_ls="aws ec2 describe-instances | jq '.Reservations[].Instances[] | {PublicDnsName, InstanceId, KeyName, PrivateDnsName}'"
alias aws_ni='aws ec2 create-tags --resources `aws ec2 run-instances --image-id ami-60b6c60a --instance-type t2.small --security-groups normal --key-name JSD-CVDMC | jq -r ".Instances[0].InstanceId"` --tags "Key=Name,Value=devbox"'
#alias aws_ni="aws ec2 run-instances --image-id $AMAZON_PV_AMI_IMAGE --key-name JSD-CVDMC --count 1 --instance-type t1.micro --security-groups normal"
ixr() { curl -F 'f:1=<-' http://ix.io < "${1:-/dev/stdin}"; }

# haste() { ( echo "% $@"; eval "$@" ) | curl -F "$@=<-" http://hastebin.com/documents|awk -F '"' '{print "http://hastebin.com/"$4}'}'"'}
