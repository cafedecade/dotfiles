#
# User configuration sourced by interactive shells
#

# Source zim
if [[ -s ${ZDOTDIR:-${HOME}}/.zim/init.zsh ]]; then
  source ${ZDOTDIR:-${HOME}}/.zim/init.zsh
fi

# Add /usr/local/bin to path.
path=(
  $HOME/.node/bin
   $HOME/bin
   $HOME/go/bin
  /usr/local/{bin,sbin}
  $path
)
#
# Less
#

# Load AWS credentials
if [ -f ~/.aws/aws_variables ]; then
  source ~/.aws/aws_variables
fi

if [ -f ~/.secrets ]; then
  source ~/.secrets
fi

# Add a .zsh-funtions. Probably should move all my various funcs into here
# Customize to your needs...
# Stuff that works on bash or zsh
if [ -r ~/.sh_aliases ]; then
  source ~/.sh_aliases
fi

# Stuff only tested on zsh, or explicitly zsh-specific
if [ -r ~/.zsh_aliases ]; then
  source ~/.zsh_aliases
fi

if [ -r ~/.zsh_functions ]; then
  source ~/.zsh_functions
fi

if [ -r ~/.zshenv ]; then
  source ~/.zshenv
fi

# Load any custom zsh completions we've installed
if [ -d ~/.zsh-completions ]; then
  for completion in ~/.zsh-completions/*
  do
    source "$completion"
  done
fi

 mkdir -p ~/.zshrc.d
 if [ -n "$(ls ~/.zshrc.d)" ]; then
   for dotfile in ~/.zshrc.d/*
   do
     if [ -r "${dotfile}" ]; then
       source "${dotfile}"
     fi
   done
fi



## Config ZAW

source $HOME/.zmods/zaw/zaw.zsh
bindkey '^R' zaw-history
bindkey -M filterselect '^R' down-line-or-history
bindkey -M filterselect '^S' up-line-or-history
bindkey -M filterselect '^E' accept-search

zstyle ':filter-style' color 'yes'
zstyle ':filter-select:highlight' matched fg=green
zstyle ':filter-select' max-lines 10
zstyle ':filter-select' rotate-list 'yes'
zstyle ':filter-select' case-insensitive 'yes'
zstyle ':filter-select' extended-search 'yes'

prompt jsd
# added by travis gem
[ -f /Users/joe/.travis/travis.sh ] && source /Users/joe/.travis/travis.sh
