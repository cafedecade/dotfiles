#
# Gitster theme
# https://github.com/shashankmehta/dotfiles/blob/master/thesetup/zsh/.oh-my-zsh/custom/themes/gitster.zsh-theme
#

jsd_get_status() {
  print "%(?:%F{10}»:%F{9}×%s)"
}

jsd_get_pwd() {
  prompt_short_dir="$(short_pwd)"
  git_root="$(command git rev-parse --show-toplevel 2> /dev/null)" && \
  prompt_short_dir="${prompt_short_dir#${$(short_pwd $git_root):h}/}"
  print ${prompt_short_dir}
}

jsd_get_virtenv() {
    [ ${VIRTUAL_ENV} ] && print "(%F{blue}${VIRTUAL_ENV:t}%f)"
}
prompt_jsd_precmd() {
  PROMPT='$(jsd_get_status)%f%{$purple%}%n%f@%{$orange%}%m:%F{118}$(jsd_get_pwd)%f $(git_prompt_info) $(jsd_get_virtenv)%(!.#.$) '
}

prompt_jsd_setup() {
  #use extended color pallete if available
  if [[ ${TERM} == *256* || ${TERM} == *rxvt* ]]; then
    turquoise="%F{81}"
    orange="%F{166}"
    purple="%F{135}"
    hotpink="%F{161}"
    limegreen="%F{118}"
  else
    turquoise="%F{cyan}"
    orange="%F{yellow}"
    purple="%F{magenta}"
    hotpink="%F{red}"
    limegreen="%F{green}"
  fi

  ZSH_THEME_GIT_PROMPT_PREFIX="%F{cyan}("
  ZSH_THEME_GIT_PROMPT_SUFFIX=")%f"
  ZSH_THEME_GIT_PROMPT_DIRTY=" %F{red}✗%f"
  ZSH_THEME_GIT_PROMPT_CLEAN=" %F{green}✓%f"

  autoload -Uz add-zsh-hook

  add-zsh-hook precmd prompt_jsd_precmd
  prompt_opts=(cr subst percent)
}

prompt_jsd_setup "$@"
